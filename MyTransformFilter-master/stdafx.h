﻿// stdafx.h : 標準のシステム インクルード ファイルのインクルード ファイル、または
// 参照回数が多く、かつあまり変更されない、プロジェクト専用のインクルード ファイル
// を記述します。
//

#pragma once

#ifndef POINTER_64  

#if !defined(_MAC) && (defined(_M_MRX000) || defined(_M_AMD64) || defined(_M_IA64)) && (_MSC_VER >= 1100) && !(defined(MIDL_PASS) || defined(RC_INVOKED))  
#define POINTER_64 __ptr64  
typedef unsigned __int64 POINTER_64_INT;
#if defined(_WIN64)  
#define POINTER_32 __ptr32  
#else  
#define POINTER_32  
#endif  
#else  
#if defined(_MAC) && defined(_MAC_INT_64)  
#define POINTER_64 __ptr64  
typedef unsigned __int64 POINTER_64_INT;
#else  
#if (_MSC_VER >= 1300) && !(defined(MIDL_PASS) || defined(RC_INVOKED))  
#define POINTER_64 __ptr64  
#else  
#define POINTER_64  
#endif  
typedef unsigned long POINTER_64_INT;
#endif  
#define POINTER_32  
#endif  
#endif

#define WIN32_LEAN_AND_MEAN		// Windows ヘッダーから使用されていない部分を除外します。
#define _WIN32_DCOM 

#pragma warning(disable : 4995)

#pragma comment(lib, "strmiids.lib")
#pragma comment(lib, "ole32.lib")
#pragma comment(lib, "winmm.lib")


#ifdef _DEBUG
#pragma comment(lib, "strmbasd.lib")
#else
#pragma comment(lib, "strmbase.lib")
#endif
//#include <dshow.h>
#include <streams.h>
#include <initguid.h>
#include <qedit.h>





// TODO: プログラムに必要な追加ヘッダーをここで参照してください。
#include "MyTransformFilter.h"
