﻿#pragma once


#define TEMPLATE_NAME	(L"My Transform Filter")
#define FILTER_NAME		("My Transform Filter")
#define INPUT_PIN_NAME  (L"Input")
#define OUTPUT_PIN_NAME (L"Output")

// {B5FFC4E9-66E8-46a7-9C2C-3ACC963D1D1B}
DEFINE_GUID(CLSID_MyTransform, 
0xb5ffc4e9, 0x66e8, 0x46a7, 0x9c, 0x2c, 0x3a, 0xcc, 0x96, 0x3d, 0x1d, 0x1b);

DEFINE_GUID(MEDIASUBTYPE_Y8,
	0x20203859, 0x0000, 0x0010, 0x80, 0x00, 0x00, 0xAA, 0x00, 0x38, 0x9B, 0x71);

// {70DEAD6B-B88B-4210-901A-F8B9C877AD34}
//DEFINE_GUID(IID_ICjSampleGrabberCB,
//	0x70dead6b, 0xb88b, 0x4210, 0x90, 0x1a, 0xf8, 0xb9, 0xc8, 0x77, 0xad, 0x34);

// {F4459A61-0C32-4E21-82CC-984E4D04849A}
//DEFINE_GUID(IID_MyTransform,
//	0xf4459a61, 0xc32, 0x4e21, 0x82, 0xcc, 0x98, 0x4e, 0x4d, 0x4, 0x84, 0x9a);



//MIDL_INTERFACE("70DEAD6B-B88B-4210-901A-F8B9C877AD34")
//ICjSampleGrabberCB : public IUnknown
//{
//public:
//	virtual HRESULT STDMETHODCALLTYPE SampleCB(
//		double SampleTime,
//		IMediaSample *pSample) = 0;
//
//	virtual HRESULT STDMETHODCALLTYPE BufferCB(
//		double SampleTime,
//		BYTE *pBuffer,
//		long BufferLen) = 0;
//
//};
//
//MIDL_INTERFACE("F4459A61-0C32-4E21-82CC-984E4D04849A")
//ICjSampleGrabber : public IUnknown
//{
//public:
//	virtual HRESULT STDMETHODCALLTYPE SetOneShot(
//		BOOL OneShot) = 0;
//
//	virtual HRESULT STDMETHODCALLTYPE SetMediaType(
//		const AM_MEDIA_TYPE *pType) = 0;
//
//	virtual HRESULT STDMETHODCALLTYPE GetConnectedMediaType(
//		AM_MEDIA_TYPE *pType) = 0;
//
//	virtual HRESULT STDMETHODCALLTYPE SetBufferSamples(
//		BOOL BufferThem) = 0;
//
//	virtual HRESULT STDMETHODCALLTYPE GetCurrentBuffer(
//		/* [out][in] */ long *pBufferSize,
//		/* [out] */ long *pBuffer) = 0;
//
//	virtual HRESULT STDMETHODCALLTYPE GetCurrentSample(
//		/* [retval][out] */ IMediaSample **ppSample) = 0;
//
//	virtual HRESULT STDMETHODCALLTYPE SetCallback(
//		ICjSampleGrabberCB *pCallback,
//		long WhichMethodToCallback) = 0;
//
//};

// ピンタイプの定義
const AMOVIESETUP_MEDIATYPE sudPinTypes =
{
    &MEDIATYPE_Video,        // Major type
    //&MEDIASUBTYPE_RGB32,  // Minor type
	&MEDIASUBTYPE_Y8
};

const AMOVIESETUP_MEDIATYPE sudPinTypesOut =
{
	&MEDIATYPE_Video,        // Major type
	//&MEDIASUBTYPE_RGB32,  // Minor type
	&MEDIASUBTYPE_YUY2
};

// 入力用、出力用ピンの情報
const AMOVIESETUP_PIN sudPins[] =
{
	{
		INPUT_PIN_NAME,
		FALSE,
		FALSE,
		FALSE,
		FALSE,
		&CLSID_NULL,
		NULL,
		1,
		&sudPinTypes
	},
	{
		OUTPUT_PIN_NAME,
		FALSE,
		TRUE,
		FALSE,
		FALSE,
		&CLSID_NULL,
		NULL,
		1,
		&sudPinTypesOut
	},
};

// フィルタ情報
const AMOVIESETUP_FILTER afFilterInfo=
{
    &CLSID_MyTransform,
	TEMPLATE_NAME,
    MERIT_DO_NOT_USE,
    2,
    sudPins
};

// トランスフォームフィルタ
class CMyTransform : public CTransformFilter{
public:
	CMyTransform(LPUNKNOWN pUnk,HRESULT *phr);
	~CMyTransform();

    static CUnknown * WINAPI	CreateInstance(LPUNKNOWN pUnk, HRESULT *phr);
	HRESULT CheckInputType(const CMediaType *mtIn);
	HRESULT GetMediaType(int iPosition, CMediaType *pMediaType);
	HRESULT CheckTransform(const CMediaType *mtIn, const CMediaType *mtOut);
	HRESULT DecideBufferSize(IMemAllocator *pAlloc, ALLOCATOR_PROPERTIES *ppropInputRequest);
	HRESULT Transform(IMediaSample *pIn, IMediaSample *pOut);
	HRESULT CompleteConnect(PIN_DIRECTION direction, IPin *pReceivePin);


protected:
private:
	VIDEOINFOHEADER	    m_Vih;

};
